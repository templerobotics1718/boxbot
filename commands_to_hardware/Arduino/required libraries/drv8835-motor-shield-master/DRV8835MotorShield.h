#ifndef DRV8835MotorShield_h
#define DRV8835MotorShield_h

#if defined(__AVR_ATmega168__) || defined(__AVR_ATmega328P__) || defined (__AVR_ATmega32U4__)
  #define DRV8835MOTORSHIELD_USE_20KHZ_PWM
#endif

#include <Arduino.h>

class DRV8835MotorShield
{
  public:
    static void setM1Speed(int speed);
    static void setM2Speed(int speed);
	static void setM3Speed(int speed);
	static void setM4Speed(int speed);
    static void setSpeeds(int m1Speed, int m2Speed, int m3Speed, int m4speed);
    static void flipM1(boolean flip);
    static void flipM2(boolean flip);
	static void flipM3(boolean flip);
	static void flipM4(boolean flip);
  
  private:
    static void initPinsAndMaybeTimer();
    static const unsigned char _M1DIR;
    static const unsigned char _M2DIR;
	static const unsigned char _M3DIR;
	static const unsigned char _M4DIR;
    static const unsigned char _M1PWM;
    static const unsigned char _M2PWM;
	static const unsigned char _M3PWM;
	static const unsigned char _M4PWM;
    static boolean _flipM1;
    static boolean _flipM2;
	static boolean _flipM3;
	static boolean _flipM4;
    
    static inline void init()
    {
      static boolean initialized = false;

      if (!initialized)
      {
        initialized = true;
        initPinsAndMaybeTimer();
      }
    }
};
#endif