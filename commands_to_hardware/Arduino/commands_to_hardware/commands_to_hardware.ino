#include <Encoder.h>
#include <PID_v1.h>
#include <DRV8835MotorShield.h>
#include <PID_v1.h>
#define LOOPTIME 100

DRV8835MotorShield motors;

//encoder intialize
//
Encoder BR(9,10); //back right M1
int BRc = 0; //counter for each wheel
Encoder FR(5,6); //front right M2
int FRc = 0;
Encoder BL(7,8); //back left M3
int BLc = 0; 
Encoder FL(2,3); //front left M4 
int FLc = 0; 

signed long velbackright; //BR
signed long velfrontright; //FR
signed long velbackleft; //BL
signed long velfrontleft;  //FL
unsigned long starttime;
unsigned long endtime;

//serial coms variables
//
String str;
int L=0;
int R=0;
int oldL = 1;
int oldR = 1;
const byte numChars = 32;
char receivedChars[numChars];
char tempChars[numChars]; // temporary array for use when parsing
char messageFromPC[numChars] = {0}; // variables to hold the parsed data
boolean newData = false;

unsigned long last_time = 0;

void setup()
{
  //set all encoders initially to zero
  //
  BR.write(0);
  FR.write(0);
  BL.write(0);
  FL.write(0);

  Serial.begin(115200);
}

void loop() {
  
  recvWithStartEndMarkers();
  if (newData == true) {
      strcpy(tempChars, receivedChars);
        parseData();
        newData = false;
    }
  
  //send motor speeds to hardware
  //only on new value (changed this to do it in Python)
  //set stop first off
  //
  if(L == 0 && R == 0){
    motors.setSpeeds(0,0,0,0);
  }
  //spin the wheels with corrected speed
  else{
    motors.setSpeeds(R,R,L,L);
  }

  unsigned long current_time = millis();
  
  if((current_time - last_time) >= LOOPTIME)
  {
    //get clicks per LOOPTIME only once every 
    //LOOPTIME
    //
    BRc = BR.read();
    FRc = FR.read();
    BLc = BL.read();
    FLc = FL.read();

    velbackright = BRc*10;
    velfrontright = FRc*10;
    velbackleft = BLc*10;
    velfrontleft = FLc*10;

    //send the data back formatted like BR,FR,BL,FL in clicks/sec
    //
    Serial.print(velbackright); Serial.print(",");
    Serial.print(velfrontright); Serial.print(",");
    Serial.print(velbackleft); Serial.print(",");
    Serial.println(velbackleft); 

    //reset encoders
    //
    BRc = 0;
    FRc = 0;
    BLc = 0;
    FLc = 0;
    BR.write(0);
    FR.write(0);
    BL.write(0);
    FL.write(0);

    //update time
    //
    last_time = millis();
  }

}

void recvWithStartEndMarkers() {
    //recieves a stream of chars formatted as: <int,int>
    //will only read when serial data is available.
    //
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;
    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();
        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }
        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}


void parseData() {
    //parse the string on , into two ints for the
    //motors
    //
    char * strtokIndx; 
    strtokIndx = strtok(tempChars,",");     
    L = atoi(strtokIndx);
    strtokIndx = strtok(NULL, ","); 
    R = atoi(strtokIndx);    
}

