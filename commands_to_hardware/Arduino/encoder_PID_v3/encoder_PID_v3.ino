#include <Encoder.h>
#include <PID_v1.h>
#include <DRV8835MotorShield.h>
#define LOOPTIME 100
#define USE_USBCON



DRV8835MotorShield motors;

//encoder intialize
//
Encoder BR(9,10); //back right M1
int BRc = 0; //counter for each wheel

Encoder FR(5,6); //front right M2
int FRc = 0;

Encoder BL(7,8); //back left M3
int BLc = 0; 

Encoder FL(2,3); //front left M4 
int FLc = 0; 

signed long velbackright; //BR
signed long velfrontright; //FR
signed long velbackleft; //BL
signed long velfrontleft;  //FL
unsigned long starttime;
unsigned long endtime;

//PID setup
//
double Setpoint_BR, Input_BR, Output_BR;
double Setpoint_FR, Input_FR, Output_FR;
double Setpoint_BL, Input_BL, Output_BL;
double Setpoint_FL, Input_FL, Output_FL;

//Specify the links and initial tuning parameters
//
double Kp=0.05, Ki=0.3, Kd=0; 

PID PID_BR(&Input_BR, &Output_BR, &Setpoint_BR, Kp, Ki, Kd, DIRECT);
PID PID_FR(&Input_FR, &Output_FR, &Setpoint_FR, Kp, Ki, Kd, DIRECT);
PID PID_BL(&Input_BL, &Output_BL, &Setpoint_BL, Kp, Ki, Kd, DIRECT);
PID PID_FL(&Input_FL, &Output_FL, &Setpoint_FL, Kp, Ki, Kd, DIRECT);

//void process_data(const std_msgs::Float32MultiArray& data)
//{
//  if (abs(data.data[0]) > 7500 || abs(data.data[1]) > 7500)
//  {
//    // Command invalid, runs outside constraints
//    Setpoint_BR = 0;
//    Setpoint_FR = 0;
//    Setpoint_FL = 0;
//    Setpoint_BL = 0;
//    
//  }
//  else
//  { 
//    Setpoint_BR = data.data[1];
//    Setpoint_FR = data.data[1];
//    Setpoint_FL = data.data[0];
//    Setpoint_BL = data.data[0];
//
//  }
//
//
//}


// create ROS node object
//ros::NodeHandle nh;

// Empty variables to populate for publishing
//std_msgs::Float64 lwheel_angular_vel;
//std_msgs::Float64 rwheel_angular_vel;
// Start publisher and subscriber objects, left side and right side
//ros::Publisher lwheel_enc("lwheel_angular_vel_enc", &lwheel_angular_vel);
//ros::Publisher rwheel_enc("rwheel_angular_vel_enc", &rwheel_angular_vel);
//ros::Subscriber<std_msgs::Float32MultiArray> sub("process_input_commands", &process_data);

//serial coms variables
//
String str;
int L=0;
int R=0;
int oldL = 1;
int oldR = 1;
const byte numChars = 32;
char receivedChars[numChars];
char tempChars[numChars]; // temporary array for use when parsing

// variables to hold the parsed data
char messageFromPC[numChars] = {0};
boolean newData = false;



void setup()
{
  //set all encoders initially to zero
  //
  BR.write(0);
  FR.write(0);
  BL.write(0);
  FL.write(0);

  //initialize linked PID variables
  //
  Input_BR = BR.read();
  Input_FR = FR.read();
  Input_BL = BL.read();
  Input_FL = FL.read();
  
  //the below set points are desired ang vel
  //1200 clicks/sec ~ 2pi rads/sec
  //
  //Setpoint_BR = 1200;
  //Setpoint_FR = 1200;
  //Setpoint_BL = 1200;
  //Setpoint_FL = 1200;

  PID_BR.SetMode(AUTOMATIC);
  PID_FR.SetMode(AUTOMATIC);
  PID_BL.SetMode(AUTOMATIC);
  PID_FL.SetMode(AUTOMATIC);
  PID_BR.SetOutputLimits(-400,400);
  PID_FR.SetOutputLimits(-400,400);
  PID_BL.SetOutputLimits(-400,400);
  PID_FL.SetOutputLimits(-400,400);

  // Initialize Node
  //nh.getHardware()->setBaud(115200);
  //nh.initNode();
  //nh.advertise(lwheel_enc);
  //nh.advertise(rwheel_enc);
  //nh.subscribe(sub);

  Serial.begin(115200);
}

void loop() {
  //nh.spinOnce();
  //wait for LOOPTIME
  //

  recvWithStartEndMarkers();
  if (newData == true) {
      strcpy(tempChars, receivedChars);
        parseData();
        //showParsedData();
        newData = false;
    }

  if(oldL != L || oldR != R){
    Setpoint_BR = R;
    Setpoint_FR = R;
    Setpoint_FL = L;
    Setpoint_BL = L;
    //motors.setSpeeds(R,R,L,L);
  }
  else if(L == 0 && R == 0){
    motors.setSpeeds(0,0,0,0);
  }

  
  starttime = millis();
  endtime = starttime;
  while((endtime - starttime) <=LOOPTIME)
  {
    endtime = millis();
  }

  //get clicks per LOOPTIME
  //
  BRc = BR.read();
  FRc = FR.read();
  BLc = BL.read();
  FLc = FL.read();

  velbackright = BRc*10;
  velfrontright = FRc*10;
  velbackleft = BLc*10;
  velfrontleft = FLc*10;

  //reset encoders
  //
  BRc = 0;
  FRc = 0;
  BLc = 0;
  FLc = 0;
  BR.write(0);
  FR.write(0);
  BL.write(0);
  FL.write(0);

  //do PID loop control
  //
  Input_BR = velbackright;
  PID_BR.Compute();
  Input_FR = velfrontright;
  PID_FR.Compute();
  Input_BL = velbackleft;
  PID_BL.Compute();
  Input_FL = velfrontleft;
  PID_FL.Compute();

  //spin the wheels with corrected speed
  //setSpeeds(BR, FR, BL, FL) and must be val -400 to 400
  //
  motors.setSpeeds(Output_BR,Output_FR,Output_BL,Output_FL);
  
  // Populate ROS message and Publish
  //lwheel_angular_vel.data = ((velfrontleft + velbackleft) / 2);
  //lwheel_enc.publish(&lwheel_angular_vel);
  //rwheel_angular_vel.data = ((velfrontright + velbackright) / 2);
  //rwheel_enc.publish(&rwheel_angular_vel);



  //print speeds to terminal
  //
  //Serial.println(velfrontleft);
  //Serial.print(",");
 // Serial.println(velfrontright);
  //Serial.print(",");
  //Serial.print("BL = "); Serial.println(velbackleft);
  //Serial.print("FL = "); Serial.println(velfrontleft);
}

void recvWithStartEndMarkers() {
    //recieves a stream of chars formatted as: <int,int>
    //will only read when serial data is available.
    //
    
    static boolean recvInProgress = false;
    static byte ndx = 0;
    char startMarker = '<';
    char endMarker = '>';
    char rc;

    while (Serial.available() > 0 && newData == false) {
        rc = Serial.read();

        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= numChars) {
                    ndx = numChars - 1;
                }
            }
            else {
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                newData = true;
            }
        }

        else if (rc == startMarker) {
            recvInProgress = true;
        }
    }
}


void parseData() {
    //parse the string on , into two ints for the
    //motors
    //

    char * strtokIndx; 

    strtokIndx = strtok(tempChars,",");     
    L = atoi(strtokIndx);
 
    strtokIndx = strtok(NULL, ","); 
    R = atoi(strtokIndx);    

}
