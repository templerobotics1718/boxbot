#!/usr/bin/env python

import rospy
from std_msgs.msg import String
from std_msgs.msg import Int32MultiArray
from geometry_msgs.msg import Twist
import serial
import time

conv_value = 0
wheel_radius = 0
wheel_base = 0

#known bug is the nuc sometimes assigns ttyACM0 to
#the Arduino and sometimes assigns ttyACM1...
#
ser = serial.Serial('/dev/ttyACM0', 115200)
#when you open a serial port, Arduino will reset
#so hang tight till the Arduino resets
#
time.sleep(1)
oldstring = "<0,0>"

def get_bot_params():
    """
    Pulls data from ROS parameter server, default values measured from Boxbot and the encoders used on it
    Returns wheel_radius, wheel_base, and conv_value
    """
    wheel_radius = rospy.get_param("wheel_radius", 0.021)
    #0.042 m is diameter changed this 2/1
    wheel_base = rospy.get_param("wheel_base", 0.15716)
    # 1200 clicks/rev and 0.005236rads/clicks on Boxbot, 190.986 clicks in 1 radian
    conv_value = rospy.get_param("conversion_value", 190.986)

    return wheel_radius, wheel_base, conv_value



def body_to_wheel_speeds(linear_velocity, angular_velocity):
    """
    Converts desired body velocities (linear and angular) to left and right side wheel velocities (rad/s)
    Returns w_left and w_right
    """
    w_left = ( (2*linear_velocity) - (wheel_base*angular_velocity) )/ (2*wheel_radius)
    w_right = ( (2*linear_velocity) + (wheel_base*angular_velocity) )/ (2*wheel_radius)

    return w_left, w_right

def exit_handler():
    """
    Safe exit of node, releases serial port
    """
    rospy.loginfo('Closing serial port...')
    ser.close()

def callback(Twist):
    """
    Parses diffdrive_controller's publish_cmd twist message, populates an Int32MultiArray message type after applying math to convert
    body velocities to left and right side wheel velocities.
    """
    linear_velocity = Twist.linear.x
    angular_velocity = Twist.angular.z
    global oldstring

    w_left, w_right = body_to_wheel_speeds(linear_velocity, angular_velocity)
    #ang_vel = [w_left, w_right] #this is in rad/sec


    #arduino accepts w in clicks/sec
    #arduino_w = [w_left*conv_value,w_right*conv_value]
    arduino_w = [(400*w_left)/33.5,(400*w_right)/33.5]
    serialString = str('<' + str(arduino_w[0]) + ',' + str(arduino_w[1]) + '>')
    #rospy.loginfo(serialString)
    
    if oldstring != serialString:
        ser.write(serialString)
        rospy.loginfo(serialString)
    oldstring = serialString        
    
    time.sleep(0.05)


def str_array_to_int_array(message):
    """
    Serial turns a string array, convert to int array
    """
    array = []
    message = message.split(',')
    for member in message:
            num = int(member)
            num = (num / conv_value) #hope this works
            array.append(num)
    return array





def main():
    """
    Starts subscriber, polls for parameters, and publishes to the /wheel_angular_vel_enc topic
    """
    global wheel_base
    global wheel_radius
    global conv_value

    wheel_radius, wheel_base, conv_value = get_bot_params()
    rospy.init_node('cmd_to_hardware', anonymous=True)
    rospy.Subscriber('/cmd_vel', Twist, callback)
    pub = rospy.Publisher('/wheel_angular_vel_enc', Int32MultiArray, queue_size=100)
    x = Int32MultiArray()
    time.sleep(1)

    while not rospy.is_shutdown():
        if ser.inWaiting() > 0:
	    read_serial = ser.readline()
	    rospy.loginfo(read_serial)
            array = str_array_to_int_array(read_serial)
            x.data = array
            pub.publish(x)
        rospy.sleep(0.05)  
    
    rospy.on_shutdown(exit_handler)

if __name__ == '__main__':
    main()
